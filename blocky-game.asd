(defsystem "blocky-game"
  :author "Anton MAsyakin <atnno@ya.ru>"
  :maintainer "Anton MAsyakin <atnno@ya.ru>"
  :license "GaPeLi"
  :version "0.0.1" ;V= release.milestone.step
  ;;:homepage "https://bg.homepage.no"
  ;;:bug-tracker "https://..."
  ;;:source-control (:git "git@github.com:CommonDoc/common-doc.git")
  :description "Minecraft clone in Common Lisp"
  :depends-on ("cffi" "cl-opengl" "sdl2" "alexandria" "cl-gamelib")
  :pathname "./"
  :components ((:module game
                        :pathname "src"
                        :components ((:file package)
                                     (:file game)
                                     (:module graphics
                                              :pathname "graphics"
                                              :components ((:file shaders)
                                                           (:file mesh)
                                                           (:file glmisc)))
                                     (:module input
                                              :pathname "input"
                                              :components ((:file input)))))))
			
