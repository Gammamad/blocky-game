;;4(in-package #:blocky-game)

(declaim (optimize (safety 0) (speed 3) (debug 0)))



;;#########################
;;#########################

(defstruct (tree-node
            ;;(:constructor)
            (:constructor make-tree-node (obj left right))
            (:print-function print-ctree))
  (object obj)
  (left nil)
  (right nil))

(defun print-ctree (obj stream level)
  (declare (ignore level))
  (format stream "(Object: ~a~& L:~a~& R:~a)"
          (tree-node-object obj)
          nil;;(tree-node-left obj)
          nil));;(tree-node-right obj)))

(defmacro branch-node-p (node)
  `(or (tree-node-left ,node)
      (tree-node-right ,node)))

(defmacro leaf-node-p (node)
  `(not (branch-node-p ,node)))

(defmacro tree-node-bb (node)
  `(if (branch-node-p ,node)
       (tree-node-object ,node)
       (object-aabb (tree-node-object ,node))))

(defmacro node-left-bb (node)
  `(tree-node-bb (tree-node-left ,node)))

(defmacro node-right-bb (node)
  `(tree-node-bb (tree-node-right ,node)))


(defmacro left-is-lessp (new left right)
  (alexandria:once-only (new left right)
  `(< (aabb-volume (aabb+ ,new ,left))
     (aabb-volume (aabb+ ,new ,right)))))

(defmacro children-sum (node)
  `(aabb+ (tree-node-bb (tree-node-left ,node))
        (tree-node-bb (tree-node-right ,node))))

(defmacro adjust-bb (node added)
  (alexandria:once-only (node)
  `(setf (tree-node-object ,node)
        (aabb+ (tree-node-bb ,node)
             ,added))))

;;(declaim (inline replace-leaf))
(declaim (ftype (function (tree-node aabb) t) replace-leaf))
(defun replace-leaf (current new)
  (let* ((obj (tree-node-object current)))
    (setf (tree-node-left current) (make-tree-node obj nil nil))
    (setf (tree-node-right current) (make-tree-node new nil nil))
    (setf (tree-node-object current) (children-sum current))))

;;(declaim (inline add-first))
(declaim (ftype (function ((or boolean tree-node) tree-node) (or boolean tree-node)) add-first))
(defun add-first (tree obj)
  (declare (type (or boolean tree-node) tree))
  (declare (type tree-node obj))
  (unless tree (setf tree obj)
          tree))

;;(declaim (inline add-next))
(declaim (ftype (function (tree-node aabb) t) add-next))
(defun add-next (current new)
  (declare (type tree-node current))
    (declare (type aabb new))
  (declare (notinline add-next))
  (if (leaf-node-p current)
      (replace-leaf current new)
      (progn
        (adjust-bb current (object-aabb new))
        (if (left-is-lessp (object-aabb new)
                           (node-left-bb current)
                           (node-right-bb current))
            (add-next (tree-node-left current) new)
            (add-next (tree-node-right current) new)))))


(declaim (ftype (function (&optional single-float single-float) aabb) gen-bb))
(defun gen-bb (&optional (box-size 10.0) (box-placement-radius 100.0))
  (declare (type single-float box-size box-placement-radius))
  (make-aabb (gv:v3 (random box-placement-radius)
                         (random box-placement-radius)
                         (random box-placement-radius))
                  (gv:v3 (random box-size)
                         (random box-size)
                         (random box-size))))

(defun bb-list(left &optional node)
  (declare (type fixnum left))
  (if (> left 0)
      (bb-list (1- left) (cons (gen-bb) node))
      node))

(declaim (ftype (function (list fixnum) fixnum) solve-naive))
(defun solve-naive (bbs sum)
  (declare (type fixnum sum))
  (if bbs
      (let ((count 0))
        (declare (type fixnum count))
        (dolist (i (cdr bbs))
          (when (aabb-intersectp (car bbs) i)
            (setf count (1+ count))))
        (solve-naive (cdr bbs) (the fixnum (+ count sum))))
      sum))

(defun check-naive (obj list)
  (let ((count 0))
    (declare (type fixnum count))
    (dolist (i list)
      (when (aabb-intersectp obj i)
        (setf count (1+ count))))
    count))

(defun entity-naive (objlist entitylist)
  (let ((list nil))
    (dolist (o objlist)
      (dolist (e entitylist)
        (when (aabb-intersectp o e)
          (push (cons e o) list))))
    list))

(defmacro next-axis(axis)
  `(mod (1+ ,axis) 3))

(defmacro prev-axis(axis)
  `(mod (+ 2 ,axis) 3))

(defun dedup (dirty &optional clean)
  (if dirty
      (let ((checked (car dirty))
            (good t))
        (dolist (i clean)
          (when (eql (cdr i) (cdr checked))
            (setf good nil)))
        (dedup (cdr dirty) (if good (cons checked clean) clean)))
      clean))

;;;General Binary tree
(declaim (ftype (function (tree-node ;;node
                           (function (tree-node t) (or null (integer -1 1))) ;;director
                           &key
                           (:extender (or null (function (tree-node t boolean) boolean))) ;;extender
                           (:leaf-visitor (or null (function (tree-node t) t))) ;;leaf visitor
                           (:state-updater (or null (function (t boolean) t)))  ;;state updater
                           (:state t)
                           (:branch-visitor (or null (function (tree-node t) t))) ;;branch visitor
                           (:last-visitor (or null (function (tree-node t) t)))) ;;last visitor
                          null)
                          bin-tree-depth-descend))
(defun bin-tree-depth-descend (node director
                               &key
                                 (extender (make-bin-tree-simple-extender))
                                 leaf-visitor
                                 state-updater
                                 state
                                 branch-visitor
                                 last-visitor)
  "Function to walk the tree
Args:
  node: tree node
  director: function to choose possible direction to move
    args: node(current tree node), state(nil or some state)
    returns: nil(stay here), -1(go left), 0(go left and right), 1(go right)
  extender: if node is leaf, but director decides to go furthe it is extender's
    responsibility to extend node to allow next step
    args: node(current tree node), state(nil or some state), side (nil left, t right)
    returns: t if extended nil otherwise and no further step done
  leaf-visitor: function to visits node if director returns nil and node is leaf
    args: node(current tree node), state(nil or some state)
  state: nil some state understood by provided director and visitors
  branch-visitor: function to visit node before next step
    args: node(current tree node), state(nil or some state)
  last-visitor: function to visit node if director returns nil but node is not leaf
    args: node(current tree node), state(nil or some state)
  state-update: updates state returning copy or original state
    args: state, direction (nil left, t right)"
  ;;(break "start")
  (declare (type tree-node node)
           (type (or null (function (tree-node t) t)) leaf-visitor branch-visitor last-visitor)
           (type (function (tree-node t boolean) boolean) extender)
           (type (function (tree-node t) (or null (integer -1 1))) director))
  (let ((direction (funcall director node state)))
    (if direction
        (progn
          (when branch-visitor
            (funcall branch-visitor node state))
          (when (<= direction 0)
            (when (or (tree-node-left node) (funcall extender node state nil))
              (bin-tree-depth-descend (tree-node-left node)
                                      director
                                      :extender extender
                                      :leaf-visitor leaf-visitor
                                      :state-updater state-updater
                                      :state (if state-updater (funcall state-updater state nil) state)
                                      :branch-visitor branch-visitor
                                      :last-visitor last-visitor)))
          (when (>= direction 0)
            (when (or (tree-node-right node) (when extender (funcall extender node state t)))
              (bin-tree-depth-descend (tree-node-right node)
                                      director
                                      :extender extender
                                      :leaf-visitor leaf-visitor
                                      :state-updater state-updater
                                      :state (if state-updater (funcall state-updater state t) state)
                                      :branch-visitor branch-visitor
                                      :last-visitor last-visitor))))
        (progn
                 ;; (break "step")
          (when (and (leaf-node-p node) leaf-visitor)
              (funcall leaf-visitor node state))
          (when last-visitor
            (funcall last-visitor node state))))))


(declaim (ftype (function ((or null (function (tree-node t) t))
                           (or null (function (tree-node t) t)))
                          (or null (function (tree-node t) t)))
                bin-tree-visitor-add))
(defun bin-tree-visitor-add(visitor-a visitor-b)
  "Returns a visitor that executes visitor-a then visitor-b
New visitor returns value of last executed visitor"
  (if (and visitor-a visitor-b)
      (let ()
        (declare (type (function (tree-node t) t) visitor-a visitor-b))
        (lambda (node state)
          (funcall visitor-a node state)
          (funcall visitor-b node state)))
      (or visitor-a visitor-b)))

(declaim (ftype (function ((or null (function (tree-node t) t))
                           (or null (function (tree-node t t) t)))
                          (or null (function (tree-node t) t)))
                bin-tree-visitor-extend))
(defun bin-tree-visitor-extend(visitor-a visitor-ext)
  "Returns a visitor that executes visitor-a then visitor-b
New visitor returns value of last executed visitor"
  (if visitor-a
      (if visitor-ext
          (let ()
            (declare (type (function (tree-node t) t) visitor-a)
                     (type (function (tree-node t t) t) visitor-ext))
            (lambda (node state)
              (funcall visitor-ext node state
                       (funcall visitor-a node state))))
          visitor-a)
      (if visitor-ext
          (let ()
            (declare (type (function (tree-node t t) t) visitor-ext))
            (lambda (node state)
              (funcall visitor-ext node state nil))))))

(declaim (ftype (function ((function (tree-node t) t)
                           (or null
                               (function (tree-node t) t)
                               (function (tree-node t t) t))
                           &optional boolean)
                          (function (tree-node t) t))
                bin-tree-visitor-append))
(defun bin-tree-visitor-append(visitor-a visitor-b &optional gets-previous-return)
  "Returns a visitor that executes visitor-a then visitor-b
New visitor returns value of last executed visitor"
  (if gets-previous-return
      (bin-tree-visitor-extend visitor-a visitor-b)
      (bin-tree-visitor-add visitor-a visitor-b)))


(declaim (ftype (function ((function (tree-node t) (or null (integer -1 1)))
                           (function (tree-node t (or null (integer -1 1))) (or null (integer -1 1))))
                          (function (tree-node t) (or null (integer -1 1))))
                bin-tree-director-extend))
(defun bin-tree-director-extend (director director-extender)
  (lambda (node state)
    (funcall director-extender node state (funcall director node state))))



(declaim (ftype (function () (function (tree-node t t) boolean)) make-bin-tree-simple-extender))
(defun make-bin-tree-simple-extender ()
  (lambda (node state side)
    (declare (type tree-node node)
             (type boolean side)
             (ignore state))
    (if side
        (progn (setf (tree-node-right node) (make-tree-node nil nil nil))
               t)
        (progn (setf (tree-node-left node) (make-tree-node nil nil nil))
               t))))
;;##########################################
(defstruct (bin-tree-seed
            (:constructor %make-bin-tree-seed(tree-name
                                         &optional (header nil))))
  (tree-name nil :type symbol)
  (header nil))

(defstruct (bin-tree (:include bin-tree-seed))
  (root (make-tree-node nil nil nil) :type tree-node)
  (insert-constructor nil :type (or null (function (t) t)))
  (remove-constructor nil :type (or null (function (t) t)))
  (search-constructor nil :type (or null (function (t) t)))
  (merge-constructor nil :type (or null (function (t) t))))

(defun grow-bin-tree (seed root &key insert-maker remove-maker search-maker merge-maker)
  (make-bin-tree :tree-name (bin-tree-seed-tree-name seed)
                 :root root
                 :header (bin-tree-seed-header seed)
                 :insert-constructor insert-maker
                 :remove-constructor remove-maker
                 :search-constructor search-maker
                 :merge-constructor merge-maker))

(defstruct (oct-tree-state
            (:constructor %make-oct-tree-state(&optional (div-p (gv:v3)) (ax 0) (adj 0.0)))
            (:copier %copy-oct-tree-state))
  (div-point div-p :type (simple-array single-float (3)))
  (axis ax :type (integer 0 2))
  (adjust adj :type single-float))

(defun make-oct-tree-state (&optional (adj 50.0)
                                (div-p (gv:v3))
                                (ax 0))
  (%make-oct-tree-state div-p ax adj))

(defun copy-oct-tree-state (state)
  (let ((div-p (copy-seq (oct-tree-state-div-point state))))
    (%make-oct-tree-state div-p (oct-tree-state-axis state) (oct-tree-state-adjust state))))

(defstruct (oct-tree-header)
  reach
  limit)

(declaim (ftype (function (t) (function (tree-node t) t)) make-oct-tree-insert-visitor))
(defun make-oct-tree-insert-visitor(object)
  (lambda (node state)
    (declare (ignore state)
             (type tree-node node))
    (push object (tree-node-object node))))
;;(setf (tree-node-object node) (cons object (tree-node-object node)))))

(defun make-oct-tree-state-updater ()
  (lambda (state direction)
    (let ((adjust (oct-tree-state-adjust state))
          (axis (oct-tree-state-axis state))
          (new-state state))
      (declare (type (integer 0 2) axis)
               (type oct-tree-state state))
      (unless direction
        (setf new-state (copy-oct-tree-state state)
              adjust (- adjust)))
      (setf (oct-tree-state-axis new-state) (next-axis axis))
      (setf (aref (oct-tree-state-div-point new-state) axis)
            (+ (aref (oct-tree-state-div-point new-state) axis) adjust))
      (when (= axis 2)
        (setf (oct-tree-state-adjust new-state)
              (/ (oct-tree-state-adjust new-state) 2)))
      new-state)))

(declaim (ftype (function (t single-float)
                          (function (tree-node t) (or null (integer -1 1))))
                make-oct-tree-director))
(defun make-oct-tree-director(object adjust-limit)
  (declare (type single-float adjust-limit))
  (let ((obj-bb (object-aabb object)))
    (lambda (node state)
      (declare (ignore node))
      (when (> (oct-tree-state-adjust state) adjust-limit)
          (aabb-axis-margin-side obj-bb
                               (oct-tree-state-axis state)
                               (aref (oct-tree-state-div-point state)
                                     (oct-tree-state-axis state)))))))

(declaim (ftype (function (t single-float)
                          (function (tree-node t) (or null (integer -1 1))))
                make-oct-tree-search-director))
(defun make-oct-tree-search-director (object adjust-limit)
  (declare (type single-float adjust-limit))
  (bin-tree-director-extend
   (make-oct-tree-director object adjust-limit)
   (lambda (node state pre-decision)
     (declare (type (or null (integer -1 1)) pre-decision)
              (ignore state))
     (when pre-decision
       (let ((left (and (<= pre-decision 0) (tree-node-left node)))
             (right (and (>= pre-decision 0) (tree-node-right node))))
         (when (or left right)
           (+ (if left -1 0)
              (if right 1 0))))))))

(defun make-oct-tree-insert(tree-seed root &key user-leaf leaf-get-return
                                             user-branch branch-get-return
                                             user-last last-get-return)
  (let ((extender (make-bin-tree-simple-extender))
        (state-updater (make-oct-tree-state-updater))
        (tree-reach (oct-tree-header-reach (bin-tree-seed-header tree-seed)))
        (adjust-limit (oct-tree-header-limit (bin-tree-seed-header tree-seed))))
    (lambda (obj)
      (let ((state (make-oct-tree-state tree-reach))
            (leaf-visitor (bin-tree-visitor-append
                           (make-oct-tree-insert-visitor obj)
                           user-leaf leaf-get-return))
            (director (make-oct-tree-director obj adjust-limit)))
        (bin-tree-depth-descend root director
                                :extender extender
                                :leaf-visitor leaf-visitor
                                :state-updater state-updater
                                :state state
                                :branch-visitor user-branch
                                :last-visitor user-last)))))

(defun make-oct-tree-search(tree-seed root &key user-leaf leaf-get-return
                                             user-branch branch-get-return
                                             user-last last-get-return)
  (let ((state-updater (make-oct-tree-state-updater))
        (tree-reach (oct-tree-header-reach (bin-tree-seed-header tree-seed)))
        (adjust-limit (oct-tree-header-limit (bin-tree-seed-header tree-seed))))
    (lambda (obj visitor)
      (let ((leaf-visitor (bin-tree-visitor-append visitor user-leaf leaf-get-return)))
        (let ((state (make-oct-tree-state tree-reach))
              (director (make-oct-tree-search-director obj adjust-limit)))
          (bin-tree-depth-descend root director
                                  :leaf-visitor leaf-visitor
                                  :state-updater state-updater
                                  :state state
                                  :branch-visitor user-branch
                                  :last-visitor user-last))))))

(defun grow-oct-tree (tree-seed)
  (let ((root (make-tree-node nil nil nil))
        (reach (oct-tree-header-reach (bin-tree-seed-header tree-seed))))
;;        (limit (oct-tree-header-limit (bin-tree-seed-header tree-seed))))
    (let ((insert-maker (lambda (&key user-leaf leaf-get-return
                                   user-branch branch-get-return
                                   user-last last-get-return)
                          (make-oct-tree-insert tree-seed root :user-leaf user-leaf :leaf-get-return leaf-get-return
                                                               :user-branch user-branch :branch-get-return branch-get-return
                                                               :user-last user-last :last-get-return last-get-return)))
          (search-maker (lambda (&key user-leaf leaf-get-return
                                   user-branch branch-get-return
                                   user-last last-get-return)
                          (make-oct-tree-search tree-seed root :user-leaf user-leaf :leaf-get-return leaf-get-return
                                                               :user-branch user-branch :branch-get-return branch-get-return
                                                               :user-last user-last :last-get-return last-get-return))))
      (let ((tree (grow-bin-tree tree-seed root
                                 :insert-maker insert-maker
                                 :search-maker search-maker)))
        tree))))

(defun make-oct-tree (reach limit)
  (grow-oct-tree (%make-bin-tree-seed 'oct-tree (make-oct-tree-header :reach reach :limit limit))))

(defun fill-mod-oct-tree(tree objects)
  (let ((adder (funcall (bin-tree-insert-constructor tree))))
    (dolist (i objects)
      (funcall adder i)))
  tree)

(defun solve-oct (bbs sum)
  (declare (type fixnum sum))
  (let ((tree (make-oct-tree 50.0 2.0)))
    (labels ((simple-check (item list sum)
               ;;(if list
                   (dolist (i list)
                     (when (aabb-intersectp (object-aabb i) (object-aabb item))
                       (setf sum (1+ sum))));;)
               sum)
             (visitor (node state)
               (let ((list (tree-node-object node)))
                 (setf sum (+ sum (simple-check (car list) (cdr list) 0))))))
      (let ((adder (funcall (bin-tree-insert-constructor tree) :user-leaf #'visitor)))
        (declare (type (or null (function (t) t)) adder))
        (when adder
          (dolist (i bbs)
            (funcall adder i)))
        sum))))

(defun entity-oct (tree entities)
  (let ((list nil)
        (pass nil))
    (flet((simple-check (item list)
            (dolist (i list)
              (when (aabb-intersectp (object-aabb item) (object-aabb i))
                (push (cons item i) pass)))))
      (let ((checker (funcall (bin-tree-search-constructor tree)))) ;;:user-leaf #'result-grabber)))
        (declare (type (or null (function (t (function (tree-node t) t)) t)) checker))
        (when checker
          (dolist (e entities)
            (funcall checker e (lambda (node state) (declare (ignore state))
                                 (simple-check e (tree-node-object node))))
            (setf list (nconc (dedup pass) list)
                  pass nil)))
        list))))

(defun compare (objects &optional (entities nil) objects-l entities-l)
  (unless objects-l (setf objects-l (bb-list objects)))
  (unless entities-l (setf entities-l (when (integerp entities) (bb-list entities))))
  (when (<= (length objects-l) 5000)
    (format t "naive ~A~%~%"
            (time (solve-naive objects-l 0))))
  (format t "Modular-oct ~A~%~%"
          (time (solve-oct objects-l 0 )))

  (format t "######################################~%")
  (when entities
    (when (<= (* (length objects-l) (length entities-l)) 100000000)
      (format t "ENTITY naive ~A~%~%"
              (time (length (entity-naive objects-l entities-l)))))
    (format t "ENTITY Modular-oct tree ~A~%~%"
            (let* ((tree (make-oct-tree 50.0 2.0)))
              (fill-mod-oct-tree tree objects-l)
              (time (length (entity-oct tree entities-l)))))))

(defun profifi (objects &optional (entities nil))
  (let((objects-l (bb-list objects))
       (entities-l (bb-list entities)))
    (sb-sprof:with-profiling (:max-samples 100000
                              :loop nil
                              :report :flat
                              :sample-interval 0.000001)
      (let* ((tree (make-oct-tree 50.0 2.0)))
                (fill-mod-oct-tree tree objects-l)
        (time (length (entity-oct tree entities-l)))))))

;;(defvar olist (bb-list 10000))
;;(defvar elist (bb-list 10000))
;;(setf olist (bb-list 10000))
;;(setf elist (bb-list 10000))
;;(compare 0 0 olist elist)
