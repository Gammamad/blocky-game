(defpackage :blocky-game/physics/aabb
  (:use :cl :cl-gamelib-vector)
  (:export :aabb :make-aabb :aabb-side :aabb+
           :aabb-size :aabb-volume
           :aabb-volume-< :aabb-intersectp
           :aabb-move :aabb-axis-devidesp
           :aabb-axis-margin-side))

(in-package :blocky-game/physics/aabb)


(defstruct (aabb
            (:constructor %make-aabb(&optional (a gv:+vector3-0+)
                                         (b gv:+vector3-0+))))
  (lower a :type gv:v3)
  (upper b :type gv:v3))

(declaim (inline make-aabb))
(declaim (ftype (function (gv:v3 gv:v3) aabb) make-aa-bb))
(defun make-aabb(origin size)
  (%make-aabb origin (gv:v3+ origin size)))

;; (declaim (inline object-aabb))
;; (declaim (ftype (function (aabb) aabb) object-aabb))
;; (defun object-aabb (obj)
;;   (declare (type aabb obj))
;;   obj)


(defmacro aabb-side (side bb)
  (let ((side (case side
                ((left right) 0)
                ((down up) 1)
                ((back front) 2)))
        (lower (or (eq side 'left)
                    (eq side 'down)
                    (eq side 'back))))
    (if lower
        `(aref (aabb-lower ,bb) ,side)
        `(aref (aabb-upper ,bb) ,side))))


(declaim (inline aabb+))
(declaim (ftype (function (aabb aabb) aabb) aabb+))
(defun aabb+ (a b)
  (declare (type aabb a b))
  (let ((left-side (min (aabb-side left a)
                        (aabb-side left b)))
        (right-side (max (aabb-side right a)
                         (aabb-side right b)))
        (back-side (min (aabb-side back a)
                        (aabb-side back b)))
        (front-side (max (aabb-side front a)
                         (aabb-side front b)))
        (down-side (min (aabb-side down a)
                        (aabb-side down b)))
        (up-side (max (aabb-side up a)
                      (aabb-side up b))))
    (%make-aabb
     (gv:v3 left-side down-side back-side)
     (gv:v3 right-side up-side front-side))))

(defmacro aabb-size (bb axis)
  (unless (integerp axis)
    (setf axis (case axis
                 ((x) 0)
                 ((y) 1)
                 ((z) 2))))
  (setf axis (mod axis 3))
  `(let ((lower (aabb-lower ,bb))
         (upper (aabb-upper ,bb)))
     (- (aref upper ,axis) (aref lower ,axis))))

(declaim (inline aabb-volume))
(declaim (ftype (function (aabb) single-float) aabb-volume))
(defun aabb-volume (bb)
  (declare (type aabb bb))
  (* (aabb-size bb x) (aabb-size bb y) (aabb-size bb z)))

(defmacro aabb-volume-< (a b)
  `(< (aabb-volume ,a)
      (aabb-volume ,b)))

(declaim (inline aabb-intersectp))
(declaim (ftype (function (aabb aabb) boolean) aabb-intersectp))
(defun aabb-intersectp (a b)
  (labels ((between (x a b)
             (and (<= a x) (>= b x))))
    (declare (inline between))
    (macrolet ((side-intersect (side-a side-b)
                 `(let ((aa (aabb-side ,side-a a))
                        (ab (aabb-side ,side-a b))
                        (ba (aabb-side ,side-b a))
                        (bb (aabb-side ,side-b b)))
                    (or (between aa ab bb)
                        (between ba ab bb)
                        (between ab aa ba)
                        (between bb aa ba)))))
      (and (side-intersect left right)
           (side-intersect down up)
           (side-intersect back front)))))


(declaim (inline aabb-move))
(declaim (ftype (function (aabb gv:v3) t) aabb-move))
(defun aabb-move (bb movement)
  (declare (type aabb bb)
           (type gv:v3 movement))
  (setf (aabb-lower bb) (gv:v3+ (aabb-lower bb) movement))
  (setf (aabb-upper bb) (gv:v3+ (aabb-upper bb) movement)))

(declaim (inline aabb-axis-devidesp))
(declaim (ftype (function (fixnum aabb aabb) boolean) aabb-axis-devidesp))
(defun aabb-axis-devidesp (axis a b)
  (or (< (aref (aabb-upper a) axis) (aref (aabb-lower b) axis))
      (< (aref (aabb-upper b) axis) (aref (aabb-lower a) axis))))

(defun aabb-axis-margin-side (bb axis margin)
  (+ (if (< (aref (aabb-lower bb) axis) margin) -1 0)
     (if (> (aref (aabb-upper bb) axis) margin) 1 0)))
