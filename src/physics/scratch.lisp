(declaim (ftype (function (&optional single-float single-float) aabb) gen-bb))
(defun gen-bb (&optional (box-size 10.0) (box-placement-radius 100.0))
  (declare (type single-float box-size box-placement-radius))
  (make-aabb (gv:v3 (random box-placement-radius)
                         (random box-placement-radius)
                         (random box-placement-radius))
                  (gv:v3 (random box-size)
                         (random box-size)
                         (random box-size))))

(defun bb-list(left &optional node)
  (declare (type fixnum left))
  (if (> left 0)
      (bb-list (1- left) (cons (gen-bb) node))
      node))

(declaim (ftype (function (list fixnum) fixnum) solve-naive))
(defun solve-naive (bbs sum)
  (declare (type fixnum sum))
  (if bbs
      (let ((count 0))
        (declare (type fixnum count))
        (dolist (i (cdr bbs))
          (when (aabb-intersectp (car bbs) i)
            (setf count (1+ count))))
        (solve-naive (cdr bbs) (the fixnum (+ count sum))))
      sum))

(defun check-naive (obj list)
  (let ((count 0))
    (declare (type fixnum count))
    (dolist (i list)
      (when (aabb-intersectp obj i)
        (setf count (1+ count))))
    count))

(defun entity-naive (objlist entitylist)
  (let ((list nil))
    (dolist (o objlist)
      (dolist (e entitylist)
        (when (aabb-intersectp o e)
          (push (cons e o) list))))
    list))

(defmacro next-axis(axis)
  `(mod (1+ ,axis) 3))

(defmacro prev-axis(axis)
  `(mod (+ 2 ,axis) 3))

(defun dedup (dirty &optional clean)
  (if dirty
      (let ((checked (car dirty))
            (good t))
        (dolist (i clean)
          (when (eql (cdr i) (cdr checked))
            (setf good nil)))
        (dedup (cdr dirty) (if good (cons checked clean) clean)))
      clean))

(defmacro node-bb (node)
  `(if (branch-p ,node)
       (node-object ,node)
       (object-aabb (node-object ,node))))

(defmacro node-left-bb (node)
  `(node-bb (node-left ,node)))

(defmacro node-right-bb (node)
  `(node-bb (node-right ,node)))

(defun fill-mod-oct-tree(tree objects)
  (let ((adder (funcall (tree-insert-constructor tree))))
    (dolist (i objects)
      (funcall adder i)))
  tree)

(defun solve-oct (bbs sum)
  (declare (type fixnum sum))
  (let ((tree (make-oct-tree 50.0 2.0)))
    (labels ((simple-check (item list sum)
               ;;(if list
                   (dolist (i list)
                     (when (aabb-intersectp (object-aabb i) (object-aabb item))
                       (setf sum (1+ sum))));;)
               sum)
             (visitor (node state)
               (let ((list (node-object node)))
                 (setf sum (+ sum (simple-check (car list) (cdr list) 0))))))
      (let ((adder (funcall (tree-insert-constructor tree) :user-leaf #'visitor)))
        (declare (type (or null (function (t) t)) adder))
        (when adder
          (dolist (i bbs)
            (funcall adder i)))
        sum))))

(defun entity-oct (tree entities)
  (let ((list nil)
        (pass nil))
    (flet((simple-check (item list)
            (dolist (i list)
              (when (aabb-intersectp (object-aabb item) (object-aabb i))
                (push (cons item i) pass)))))
      (let ((checker (funcall (tree-search-constructor tree)))) ;;:user-leaf #'result-grabber)))
        (declare (type (or null (function (t visitor) t)) checker))
        (when checker
          (dolist (e entities)
            (funcall checker e (lambda (node state) (declare (ignore state))
                                 (simple-check e (node-object node))))
            (setf list (nconc (dedup pass) list)
                  pass nil)))
        list))))

(defun compare (objects &optional (entities nil) objects-l entities-l)
  (unless objects-l (setf objects-l (bb-list objects)))
  (unless entities-l (setf entities-l (when (integerp entities) (bb-list entities))))
  (when (<= (length objects-l) 5000)
    (format t "naive ~A~%~%"
            (time (solve-naive objects-l 0))))
  (format t "Modular-oct ~A~%~%"
          (time (solve-oct objects-l 0 )))

  (format t "######################################~%")
  (when entities
    (when (<= (* (length objects-l) (length entities-l)) 100000000)
      (format t "ENTITY naive ~A~%~%"
              (time (length (entity-naive objects-l entities-l)))))
    (format t "ENTITY Modular-oct tree ~A~%~%"
            (let* ((tree (make-oct-tree 50.0 2.0)))
              (fill-mod-oct-tree tree objects-l)
              (time (length (entity-oct tree entities-l)))))))

(defun profifi (objects &optional (entities nil))
  (let((objects-l (bb-list objects))
       (entities-l (bb-list entities)))
    (sb-sprof:with-profiling (:max-samples 100000
                              :loop nil
                              :report :flat
                              :sample-interval 0.000001)
      (let* ((tree (make-oct-tree 50.0 2.0)))
                (fill-mod-oct-tree tree objects-l)
        (time (length (entity-oct tree entities-l)))))))

;;(defvar olist (bb-list 10000))
;;(defvar elist (bb-list 10000))
;;(setf olist (bb-list 10000))
;;(setf elist (bb-list 10000))
;;(compare 0 0 olist elist)
