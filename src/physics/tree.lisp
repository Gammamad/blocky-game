(uiop:define-package :blocky-game/physics/tree
  (:use :common-lisp :cl-gamelib-vector :blocky-game/physics/aabb)
   (:export :node :make-node :print-node-short :branch-p :leaf-p
           :tree-seed :make-tree-seed
           :tree :make-tree :grow-tree
           :tree-depth-descend :tree-visitor-add
           :tree-viisitor-extend :tree-visitor-append
           :tree-director-extend :make-tree-simple-extender))

(in-package :blocky-game/physics/tree)

;;;General Binary tree

;;general binary tree node
(defstruct (node
            (:constructor make-node (obj left right))
            (:print-function print-node-short))
  (object obj)
  (left nil)
  (right nil))

(defun print-node-short (obj stream level)
  (declare (ignore level))
  (format stream "(Object: ~a~& L:~a~& R:~a)"
          (node-object obj)
          nil;;(node-left obj)
          nil));;(node-right obj)))

(defmacro branch-p (node)
  `(or (node-left ,node)
       (node-right ,node)))

(defmacro leaf-p (node)
  `(not (branch-p ,node)))


(defstruct (tree-seed
            (:constructor make-tree-seed(tree-name
                                         &optional (header nil))))
  (tree-name nil :type symbol)
  (header nil))

(defstruct (tree (:include tree-seed))
  (root (make-node nil nil nil) :type node)
  (insert-constructor nil :type (or null function))
  (remove-constructor nil :type (or null function))
  (search-constructor nil :type (or null function))
  (merge-constructor  nil :type (or null function)))

(defun grow-tree (seed root &key insert-maker remove-maker search-maker merge-maker)
  (make-tree :tree-name (tree-seed-tree-name seed)
                 :root root
                 :header (tree-seed-header seed)
                 :insert-constructor insert-maker
                 :remove-constructor remove-maker
                 :search-constructor search-maker
                 :merge-constructor merge-maker))

(deftype visitor ()
  '(function (node t) t))

(deftype visitor-extender ()
    '(function (node t t) t))

(deftype extender ()
  '(function (node t boolean) boolean))

(deftype direction ()
  '(or null (integer -1 1)))

(deftype director ()
  '(function (node t) direction))

(deftype director-extender ()
  '(function (node t direction) direction))

(deftype state-updater ()
  '(function (t boolean) t))

(declaim (ftype (function (node director
                           &key
                           (:extender (or null extender)) ;;extender
                           (:leaf-visitor (or null visitor)) ;;leaf visitor
                           (:state-updater (or null state-updater))  ;;state updater
                           (:state t)
                           (:branch-visitor (or null visitor)) ;;branch visitor
                           (:last-visitor (or null visitor))) ;;last visitor
                          null)
                          tree-depth-descend))
(defun tree-depth-descend (node director &key (extender (make-tree-simple-extender))
                                               leaf-visitor state-updater state branch-visitor last-visitor)
  "Function to walk the tree
Args:
  node: tree node
  director: function to choose possible direction to move
    args: node(current tree node), state(nil or some state)
    returns: nil(stay here), -1(go left), 0(go left and right), 1(go right)
  extender: if node is leaf, but director decides to go furthe it is extender's
    responsibility to extend node to allow next step
    args: node(current tree node), state(nil or some state), side (nil left, t right)
    returns: t if extended nil otherwise and no further step done
  leaf-visitor: function to visits node if director returns nil and node is leaf
    args: node(current tree node), state(nil or some state)
  state: nil some state understood by provided director and visitors
  branch-visitor: function to visit node before next step
    args: node(current tree node), state(nil or some state)
  last-visitor: function to visit node if director returns nil but node is not leaf
    args: node(current tree node), state(nil or some state)
  state-update: updates state returning copy or original state
    args: state, direction (nil left, t right)"
  ;;(break "start")
  (declare (type node node)
           (type (or null visitor) leaf-visitor branch-visitor last-visitor)
           (type extender extender)
           (type director director))
  (let ((direction (funcall director node state)))
    (if direction
        (progn
          (when branch-visitor
            (funcall branch-visitor node state))
          (when (<= direction 0)
            (when (or (node-left node) (funcall extender node state nil))
              (tree-depth-descend (node-left node)
                                      director
                                      :extender extender
                                      :leaf-visitor leaf-visitor
                                      :state-updater state-updater
                                      :state (if state-updater (funcall state-updater state nil) state)
                                      :branch-visitor branch-visitor
                                      :last-visitor last-visitor)))
          (when (>= direction 0)
            (when (or (node-right node) (when extender (funcall extender node state t)))
              (tree-depth-descend (node-right node)
                                      director
                                      :extender extender
                                      :leaf-visitor leaf-visitor
                                      :state-updater state-updater
                                      :state (if state-updater (funcall state-updater state t) state)
                                      :branch-visitor branch-visitor
                                      :last-visitor last-visitor))))
        (progn
                 ;; (break "step")
          (when (and (leaf-p node) leaf-visitor)
              (funcall leaf-visitor node state))
          (when last-visitor
            (funcall last-visitor node state))))))


(declaim (ftype (function ((or null visitor) (or null visitor)) (or null visitor))
                tree-visitor-add))
(defun tree-visitor-add(visitor-a visitor-b)
  "Returns a visitor that executes visitor-a then visitor-b
New visitor returns value of last executed visitor"
  (if (and visitor-a visitor-b)
      (let ()
        (declare (type visitor visitor-a visitor-b))
        (lambda (node state)
          (funcall visitor-a node state)
          (funcall visitor-b node state)))
      (or visitor-a visitor-b)))

(declaim (ftype (function ((or null visitor) (or null visitor-extender)) (or null visitor))
                tree-visitor-extend))
(defun tree-visitor-extend(visitor-a visitor-ext)
  "Returns a visitor that executes visitor-a then visitor-b
New visitor returns value of last executed visitor"
  (if visitor-a
      (if visitor-ext
          (let ()
            (declare (type visitor visitor-a)
                     (type visitor-extender visitor-ext))
            (lambda (node state)
              (funcall visitor-ext node state
                       (funcall visitor-a node state))))
          visitor-a)
      (if visitor-ext
          (let ()
            (declare (type visitor-extender visitor-ext))
            (lambda (node state)
              (funcall visitor-ext node state nil))))))

(declaim (ftype (function (visitor (or null visitor visitor-extender)
                                   &optional boolean)
                          visitor)
                tree-visitor-append))
(defun tree-visitor-append(visitor-a visitor-b &optional gets-previous-return)
  "Returns a visitor that executes visitor-a then visitor-b
New visitor returns value of last executed visitor"
  (if gets-previous-return
      (tree-visitor-extend visitor-a visitor-b)
      (tree-visitor-add visitor-a visitor-b)))


(declaim (ftype (function (director director-extender) director)
                tree-director-extend))
(defun tree-director-extend (director director-extender)
  (lambda (node state)
    (funcall director-extender node state (funcall director node state))))



(declaim (ftype (function () (function (node t t) boolean)) make-tree-simple-extender))
(defun make-tree-simple-extender ()
  (lambda (node state side)
    (declare (type node node)
             (type boolean side)
             (ignore state))
    (if side
        (progn (setf (node-right node) (make-node nil nil nil))
               t)
        (progn (setf (node-left node) (make-node nil nil nil))
               t))))


