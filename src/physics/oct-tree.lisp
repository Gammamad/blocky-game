(uiop:define-package :blocky-game/physics/oct-tree
  (:use :cl :blocky-game/physics/tree)
  (:export :make-oct-tree))


(defstruct (oct-tree-state
            (:constructor %make-oct-tree-state(&optional (div-p (gv:v3)) (ax 0) (adj 0.0)))
            (:copier %copy-oct-tree-state))
  (div-point div-p :type (simple-array single-float (3)))
  (axis ax :type (integer 0 2))
  (adjust adj :type single-float))

(defun make-oct-tree-state (&optional (adj 50.0)
                                (div-p (gv:v3))
                                (ax 0))
  (%make-oct-tree-state div-p ax adj))

(defun copy-oct-tree-state (state)
  (let ((div-p (copy-seq (oct-tree-state-div-point state))))
    (%make-oct-tree-state div-p (oct-tree-state-axis state) (oct-tree-state-adjust state))))

(defstruct (oct-tree-header)
  reach
  limit)

(declaim (ftype (function (t) visitor) make-oct-tree-insert-visitor))
(defun make-oct-tree-insert-visitor(object)
  (lambda (node state)
    (declare (ignore state)
             (type node node))
    (push object (node-object node))))
;;(setf (node-object node) (cons object (node-object node)))))

(defun make-oct-tree-state-updater ()
  (lambda (state direction)
    (let ((adjust (oct-tree-state-adjust state))
          (axis (oct-tree-state-axis state))
          (new-state state))
      (declare (type (integer 0 2) axis)
               (type oct-tree-state state))
      (unless direction
        (setf new-state (copy-oct-tree-state state)
              adjust (- adjust)))
      (setf (oct-tree-state-axis new-state) (next-axis axis))
      (setf (aref (oct-tree-state-div-point new-state) axis)
            (+ (aref (oct-tree-state-div-point new-state) axis) adjust))
      (when (= axis 2)
        (setf (oct-tree-state-adjust new-state)
              (/ (oct-tree-state-adjust new-state) 2)))
      new-state)))

(declaim (ftype (function (t single-float)
                          director)
                make-oct-tree-director))
(defun make-oct-tree-director(object adjust-limit)
  (declare (type single-float adjust-limit))
  (let ((obj-bb (object-aabb object)))
    (lambda (node state)
      (declare (ignore node))
      (when (> (oct-tree-state-adjust state) adjust-limit)
          (aabb-axis-margin-side obj-bb
                               (oct-tree-state-axis state)
                               (aref (oct-tree-state-div-point state)
                                     (oct-tree-state-axis state)))))))

(declaim (ftype (function (t single-float)
                          director)
                make-oct-tree-search-director))
(defun make-oct-tree-search-director (object adjust-limit)
  (declare (type single-float adjust-limit))
  (tree-director-extend
   (make-oct-tree-director object adjust-limit)
   (lambda (node state pre-decision)
     (declare (type direction pre-decision)
              (ignore state))
     (when pre-decision
       (let ((left (and (<= pre-decision 0) (node-left node)))
             (right (and (>= pre-decision 0) (node-right node))))
         (when (or left right)
           (+ (if left -1 0)
              (if right 1 0))))))))

(defun make-oct-tree-insert(tree-seed root &key user-leaf leaf-get-return
                                             user-branch branch-get-return
                                             user-last last-get-return)
  (let ((extender (make-tree-simple-extender))
        (state-updater (make-oct-tree-state-updater))
        (tree-reach (oct-tree-header-reach (tree-seed-header tree-seed)))
        (adjust-limit (oct-tree-header-limit (tree-seed-header tree-seed))))
    (lambda (obj)
      (let ((state (make-oct-tree-state tree-reach))
            (leaf-visitor (tree-visitor-append
                           (make-oct-tree-insert-visitor obj)
                           user-leaf leaf-get-return))
            (director (make-oct-tree-director obj adjust-limit)))
        (tree-depth-descend root director
                                :extender extender
                                :leaf-visitor leaf-visitor
                                :state-updater state-updater
                                :state state
                                :branch-visitor user-branch
                                :last-visitor user-last)))))

(defun make-oct-tree-search(tree-seed root &key user-leaf leaf-get-return
                                             user-branch branch-get-return
                                             user-last last-get-return)
  (let ((state-updater (make-oct-tree-state-updater))
        (tree-reach (oct-tree-header-reach (tree-seed-header tree-seed)))
        (adjust-limit (oct-tree-header-limit (tree-seed-header tree-seed))))
    (lambda (obj visitor)
      (let ((leaf-visitor (tree-visitor-append visitor user-leaf leaf-get-return)))
        (let ((state (make-oct-tree-state tree-reach))
              (director (make-oct-tree-search-director obj adjust-limit)))
          (tree-depth-descend root director
                                  :leaf-visitor leaf-visitor
                                  :state-updater state-updater
                                  :state state
                                  :branch-visitor user-branch
                                  :last-visitor user-last))))))

(defun grow-oct-tree (tree-seed)
  (let ((root (make-node nil nil nil))
        (reach (oct-tree-header-reach (tree-seed-header tree-seed))))
;;        (limit (oct-tree-header-limit (tree-seed-header tree-seed))))
    (let ((insert-maker (lambda (&key user-leaf leaf-get-return
                                   user-branch branch-get-return
                                   user-last last-get-return)
                          (make-oct-tree-insert tree-seed root :user-leaf user-leaf :leaf-get-return leaf-get-return
                                                               :user-branch user-branch :branch-get-return branch-get-return
                                                               :user-last user-last :last-get-return last-get-return)))
          (search-maker (lambda (&key user-leaf leaf-get-return
                                   user-branch branch-get-return
                                   user-last last-get-return)
                          (make-oct-tree-search tree-seed root :user-leaf user-leaf :leaf-get-return leaf-get-return
                                                               :user-branch user-branch :branch-get-return branch-get-return
                                                               :user-last user-last :last-get-return last-get-return))))
      (let ((tree (grow-tree tree-seed root
                                 :insert-maker insert-maker
                                 :search-maker search-maker)))
        tree))))

(defun make-oct-tree (reach limit)
  (grow-oct-tree (make-tree-seed 'oct-tree (make-oct-tree-header :reach reach :limit limit))))


;;###TESTING###
(defun fill-mod-oct-tree(tree objects)
  (let ((adder (funcall (tree-insert-constructor tree))))
    (dolist (i objects)
      (funcall adder i)))
  tree)

(defun solve-oct (bbs sum)
  (declare (type fixnum sum))
  (let ((tree (make-oct-tree 50.0 2.0)))
    (labels ((simple-check (item list sum)
               ;;(if list
                   (dolist (i list)
                     (when (aabb-intersectp (object-aabb i) (object-aabb item))
                       (setf sum (1+ sum))));;)
               sum)
             (visitor (node state)
               (let ((list (node-object node)))
                 (setf sum (+ sum (simple-check (car list) (cdr list) 0))))))
      (let ((adder (funcall (tree-insert-constructor tree) :user-leaf #'visitor)))
        (declare (type (or null (function (t) t)) adder))
        (when adder
          (dolist (i bbs)
            (funcall adder i)))
        sum))))

(defun entity-oct (tree entities)
  (let ((list nil)
        (pass nil))
    (flet((simple-check (item list)
            (dolist (i list)
              (when (aabb-intersectp (object-aabb item) (object-aabb i))
                (push (cons item i) pass)))))
      (let ((checker (funcall (tree-search-constructor tree)))) ;;:user-leaf #'result-grabber)))
        (declare (type (or null (function (t visitor) t)) checker))
        (when checker
          (dolist (e entities)
            (funcall checker e (lambda (node state) (declare (ignore state))
                                 (simple-check e (node-object node))))
            (setf list (nconc (dedup pass) list)
                  pass nil)))
        list))))

(defun compare (objects &optional (entities nil) objects-l entities-l)
  (unless objects-l (setf objects-l (bb-list objects)))
  (unless entities-l (setf entities-l (when (integerp entities) (bb-list entities))))
  (when (<= (length objects-l) 5000)
    (format t "naive ~A~%~%"
            (time (solve-naive objects-l 0))))
  (format t "Modular-oct ~A~%~%"
          (time (solve-oct objects-l 0 )))

  (format t "######################################~%")
  (when entities
    (when (<= (* (length objects-l) (length entities-l)) 100000000)
      (format t "ENTITY naive ~A~%~%"
              (time (length (entity-naive objects-l entities-l)))))
    (format t "ENTITY Modular-oct tree ~A~%~%"
            (let* ((tree (make-oct-tree 50.0 2.0)))
              (fill-mod-oct-tree tree objects-l)
              (time (length (entity-oct tree entities-l)))))))

(defun profifi (objects &optional (entities nil))
  (let((objects-l (bb-list objects))
       (entities-l (bb-list entities)))
    (sb-sprof:with-profiling (:max-samples 100000
                              :loop nil
                              :report :flat
                              :sample-interval 0.000001)
      (let* ((tree (make-oct-tree 50.0 2.0)))
                (fill-mod-oct-tree tree objects-l)
        (time (length (entity-oct tree entities-l)))))))

;;(defvar olist (bb-list 10000))
;;(defvar elist (bb-list 10000))
;;(setf olist (bb-list 10000))
;;(setf elist (bb-list 10000))
;;(compare 0 0 olist elist)
