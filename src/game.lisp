
(uiop:define-package :blocky-game/game
  (:use :cl
        :cl-gamelib-vector
        :cl-gamelib-matrix
        :cl-gamelib-quaternion)
  (:export :game))

;; This is my attempt to make minecraft clone in Common LISP
;; Wish me a good luck

(in-package :blocky-game/game)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (proclaim '(optimize (speed 2) (debug 2))))

(defun to-gl-array (contents &key (type :int))
  (let* ((length (length contents))
         (gl-array (gl:alloc-gl-array type length)))
    (loop for a in contents
       for i from 0 to length
       do(setf (gl:glaref gl-array i) a))
    gl-array))

(defvar cubeverts nil)
(setf cubeverts '(-0.5 -0.5 -0.5 0.5 0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 0.5 0.5
 -0.5 0.5 -0.5 -0.5 0.5 0.5 0.5 0.5 0.5 -0.5 0.5 -0.5 -0.5 0.5 -0.5 0.5 0.5 0.5
 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5 0.5 -0.5 0.5 -0.5 -0.5 0.5 -0.5 0.5
 0.5 -0.5 -0.5 0.5 -0.5 0.5 -0.5 -0.5 0.5 0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.5 -0.5
 0.5 -0.5 -0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 -0.5 -0.5 -0.5 0.5 0.5 -0.5 0.5
 0.5 -0.5 -0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5
 -0.5 0.5 0.5 0.5))

(defvar cubecolors nil)
(setf cubecolors '(0.5 0.0 0.0 0.0 0.0 0.7 0.0 0.5 0.0 0.5 0.0 0.0 0.5 0.5 0.5 0.0 0.0 0.7 0.5
 0.5 0.5 0.0 0.0 1.0 0.0 0.0 0.7 0.5 0.5 0.5 0.9 0.9 0.9 0.0 0.0 1.0 0.9 0.9
 0.9 0.0 0.9 0.0 0.0 0.0 1.0 0.9 0.9 0.9 0.9 0.0 0.0 0.0 0.9 0.0 0.9 0.0 0.0
 0.0 0.5 0.0 0.0 0.9 0.0 0.9 0.0 0.0 0.5 0.0 0.0 0.0 0.5 0.0 0.9 0.0 0.0 0.5
 0.5 0.5 0.5 0.0 0.0 0.9 0.0 0.0 0.9 0.9 0.9 0.5 0.5 0.5 0.0 0.5 0.0 0.0 0.0
 1.0 0.0 0.9 0.0 0.0 0.5 0.0 0.0 0.0 0.7 0.0 0.0 1.0))

(defvar cubenormals nil)
(setf cubenormals '(0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0
                    1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 0.0
                    0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 -1.0 0.0
                    0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 0.0 -1.0
                    0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 -1.0 0.0 0.0 1.0
                    0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0 0.0 1.0 0.0))

(defun prepare-cube ()
  (let* ((vao (gl:gen-vertex-array))
	 (buffer (gl:gen-buffer))
	 ;;(ibuffer (gl:gen-buffer))
         ;;(maxb 255)
         ;;(indicies (to-gl-array (list 0 1 3 2 4 6 7 1 maxb 5 4 2 3 1 7 6 4) :type :uint8 ))
         (verticies (to-gl-array cubeverts :type :float ))
	 (colors (to-gl-array cubecolors :type :float ))
         (normals (to-gl-array cubenormals :type :float)))
    ;;bind vao
    (gl:bind-vertex-array vao)
    ;; bind index buffer(vao state)
    ;;(gl:bind-buffer :element-array-buffer ibuffer)
    ;;(gl:buffer-data :element-array-buffer
;;		    :static-draw
;;		    indicies)
    ;; bind vertex buffer 
    (gl:bind-buffer :array-buffer buffer)
    (gl:buffer-data :array-buffer
		    :static-draw
		    verticies
		    :size (+ (gl:gl-array-byte-size verticies)
			     (gl:gl-array-byte-size colors)
                             (gl:gl-array-byte-size normals)))
    ;; addd color data
    (gl:buffer-sub-data :array-buffer colors
			:buffer-offset (gl:gl-array-byte-size verticies))
    (gl:enable-vertex-attrib-array 1)
    (gl:vertex-attrib-pointer 1 3
                              :float :false
                              0 (gl:gl-array-byte-size verticies))
    ;; addd normals data
    (gl:buffer-sub-data :array-buffer normals
			:buffer-offset (+ (gl:gl-array-byte-size verticies)
                                          (gl:gl-array-byte-size colors)))
    (gl:enable-vertex-attrib-array 2)
    (gl:vertex-attrib-pointer 2 3
                              :float :false
                              0 (+ (gl:gl-array-byte-size verticies)
                                          (gl:gl-array-byte-size colors)))
    ;; add buffer to vao
    (gl:enable-vertex-attrib-array 0)
    (gl:vertex-attrib-pointer 0 3 :float :false 0 0)

    ;;(%gl:primitive-restart-index maxb)
    vao))



(defun display (vao &optional (count 3))
  (gl:clear-color 0.0 0.5 0.0 1.0)
  (gl:clear :color-buffer-bit :depth-buffer-bit)

  (gl:bind-vertex-array vao)
  (gl:draw-arrays :triangles 0 count)
  (gl:bind-vertex-array 0))

(defun display-cube (vao &optional (el 3))
  (gl:bind-vertex-array vao)
;;  (gl:enable :primitive-restart)
  ;;(%gl:draw-elements :triangle-fan el :unsigned-byte 0)
  ;;(break)
  (gl:draw-arrays :triangles 0 el)
  ;;(gl:disable :primitive-restart)
  (gl:bind-vertex-array 0))

(defun f-idle (window vao color el)
  (gl:clear-color color color color 1.0)
  (gl:clear :color-buffer-bit :depth-buffer-bit)
  (display-cube vao el)
  (sdl2:gl-swap-window window))


(defun turn-matrix (direction hangle vangle)
  (let* ((yaw-axis (gv:v4 0.0 1.0 0.0 1.0))
         (pitch-axis (gv:v4 1.0 0.0 0.0 1.0))
         (yaw-quaternion (gq:q-rotation yaw-axis vangle))
         (pitch-quaternion (gq:q-rotation pitch-axis hangle)))
    (gq:q-mat (gq:q* yaw-quaternion pitch-quaternion))))


(defun camera-position-matrix (position direction head)
  (let (;;(roll-axis direction)
        (yaw-axis head)
        (pitch-axis
          (gv:v4 1.0 0.0 0.0 1.0))
        (translation (gm:translation-m4 (gv:v3-x position)
                                       (gv:v3-y position)
                                       (gv:v3-z position)))
        (zero-vector (v4 0.0 0.0 1.0 1.0))
        (yaw-vector (v4proj-xz direction)))
    (let ((yaw-angle (v3v-angle zero-vector yaw-vector))
          (pitch-angle (- (v3v-angle yaw-axis direction) (/ pi 2))))
      (let ((yaw-quaternion (gq:q-rotation yaw-axis (* yaw-angle (if (< (v4-x direction) 0) -1 1))))
            (pitch-quaternion (gq:q-rotation pitch-axis (* pitch-angle (if (< (v4-y direction) 0) 1 1)))))
        (gm:m4* (gq:q-mat (gq:q* yaw-quaternion pitch-quaternion ))
                translation)))))


(defun move-direction (view-direction head &optional (forward-velocity 0.0) (side-velocity 0.0) (head-velocity 0.0))
  (flet ((thres-nil (f) (when (> (abs f) 0.0001) f)))
    (let ((side-vector (gv:v3x view-direction head)))
      (if (or (thres-nil forward-velocity) (thres-nil side-velocity) (thres-nil head-velocity))
          (progn ;;(break)
                 (v3unit (v3+ (v3scale side-vector side-velocity)
                              (v3scale view-direction forward-velocity)
                              (v3scale head head-velocity))))
          gv:+vector3-0+))))


(defun move (position movev speed)
    (gv:v3+ position (gv:v3scale movev speed)))


(defun camera-matrix (position-matrix fov aspect &key (near 0.01) (far 1000.0))
  (gm:m4* (gm:fov-m4 fov aspect near far)
          position-matrix))



;; NEW VARIANT
(defstruct uniforms
  (projection)
  (camera)
  (translation)
  (object-rotation)
  (light))

(defstruct camera
  (position (gv:v3))
  (direction (gv:v4 0.0 0.0 1.0 1.0)))

(defstruct game-env
  (camera (make-camera))
  (world)
  (key-map (make-input-map))
  (data (make-hash-table)))

(defstruct inputs
  (forward)
  (backward)
  (left)
  (right)
  (up)
  (down)
  (sprint)
  (mouse-move)
  (mouse))

(defmacro getenv (name env)
  `(gethash ,name (game-env-data ,env)))

(defmacro setenv (name env value)
  `(setf (getenv ,name ,env) ,value))

(defsetf getenv (name env) (value)
  `(setf (gethash ,name (game-env-data ,env)) ,value))

(defun go-forward (env &optional go)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-forward inputs) go)))

(defun go-backward (env &optional go)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-backward inputs) go)))

(defun go-left (env &optional go)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-left inputs) go)))

(defun go-right (env &optional go)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-right inputs) go)))

(defun go-up (env &optional go)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-up inputs) go)))

(defun go-down (env &optional go)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-down inputs) go)))

(defun go-mouse (env x y)
  (let ((inputs (getenv 'inputs env)))
    (setf (inputs-mouse-move inputs) t))
  (let ((vx (getenv 'view-x env))
        (vy (getenv 'view-y env)))
    (setf vx (gh:clamp (+ vx (/ y 10.0)) -90 90))
    (setf vy (mod (+ vy (/ x 10.0)) 360.0))
  (setenv 'view-x env vx)
    (setenv 'view-y env vy)))


(defun init-opengl (env)
  (let ((program (prepare-shader-program
         :vertex "src/graphics/shaders/shader.vert"
         :fragment "src/graphics/shaders/shader.frag")))
    (setf (getenv 'program env) program)

    (sdl2:gl-set-swap-interval 1)

    (gl:use-program program)
    ;;(gl:polygon-mode :front-and-back :line)
    (gl:polygon-mode :front-and-back :fill)
    (gl:enable :depth-test)))

(defun init-world (env)
  (setf (getenv 'vao env) (prepare-cube)))

(defun init-input (env)
  (setenv 'inputs env (make-inputs))
  (let ((k-map (game-env-key-map env)))
    (flet ((set-map (key &key down up)
             (input-map-set k-map (sdl2:scancode-key-to-value key)
                            :down down :up up))
           (quit-evt (env down)(declare (ignore env down)) (sdl2:with-sdl-event (evt :quit) (sdl2:push-event evt))))
      (set-map :scancode-escape :down #'quit-evt)
      (set-map :scancode-w
               :down #'go-forward
               :up   #'go-forward)
      (set-map :scancode-s
               :down #'go-backward
               :up   #'go-backward)
      (set-map :scancode-a
               :down #'go-left
               :up   #'go-left)
      (set-map :scancode-d
               :down #'go-right
               :up   #'go-right))))


(defun init-vars (env)
  (setenv 'obj-rotation-x env 0.0)
  (setenv 'obj-rotation-y env 0.0)
  (setenv 'camera env (make-camera))
  (setenv 'view-x env 0.0)
  (setenv 'view-y env 0.0))

(defun init-env (env window)
  (setenv 'window env window)
  (setenv 'uniforms env (make-uniforms))
  (init-vars env)
  (init-opengl env)
  (init-world env)
  (init-input env))

(defun do-events (env &optional evt)
  (let ((k-map (game-env-key-map env))
        (quit nil))
    (labels ((kprint (scode &key down)
               (declare (ignore down))
               (format t "~&~a~%" (sdl2:scancode-key-name scode)))
             (process (env evt)
               ;;(break)
               (when (sdl2:handle-next-event (:event evt :quit quit)
                       (:keydown (:keysym keysym :repeat rep)
                                 (when (= rep 0)
                                   (input-map-do k-map (sdl2:scancode-value keysym)
                                                 :down t :env env :printkey #'kprint)))
                       (:keyup (:keysym keysym :repeat rep)
                               (when (= rep 0)
                                 (input-map-do k-map (sdl2:scancode-value keysym)
                                               :down nil :env env :printkey #'kprint)))
                       (:mousemotion (:xrel xr :yrel yr)
                                     (go-mouse env xr yr))
                       (:quit () (format t "QUIT!~%") t))
                 (process env evt))))
      (process env evt)
      quit)))

(defun do-move (env)
  (let ((inputs (getenv 'inputs env))
        (forward 0.0)
        (side 0.0)
        (up 0.0)
        (camera (getenv 'camera env))
        (mv nil)
        (mo nil)
        (xdeg 0.0)
        (ydeg 0.0))
      (when (inputs-forward inputs) (setf forward (1+ forward)) (setf mv t))
      (when (inputs-backward inputs)  (setf forward (1- forward)) (setf mv t))
      (when (inputs-left inputs)  (setf side (1- side)) (setf mv t))
      (when (inputs-right inputs)  (setf side (1+ side)) (setf mv t))
      (when (inputs-mouse-move inputs)
        (setf xdeg (getenv 'view-x env))
        (setf ydeg (getenv 'view-y env))
        (setf (inputs-mouse-move inputs) nil)
        (setf mv t
              mo t))
    (when mv
        (let ((position (camera-position camera))
              (view-direction (camera-direction camera)))
          (when mo
              (let ((xrad (gh:deg-to-rad xdeg))
                    (yrad (gh:deg-to-rad ydeg)))
                (setf (v4-x view-direction) (coerce (* (cos yrad)(cos xrad)) 'single-float))
                (setf (v4-y view-direction) (coerce (sin xrad) 'single-float))
                (setf (v4-z view-direction) (coerce (* (sin yrad)(cos xrad)) 'single-float))))
          (setf position (move position
                               (move-direction view-direction
                                               +vector3-y+
                                               forward
                                               side
                                               up)
                               0.01))
          (setf (camera-position camera) position)
          (setenv 'camera env camera)))))

(defun do-things (env)
  (do-move env)
  (let ((uniforms (getenv 'uniforms env))
        (orx (getenv 'obj-rotation-x env))
        (ory (getenv 'obj-rotation-y env))
        (camera (getenv 'camera env)))

    (let ((rvx (gq:quat 1.0 0.0 0.0 0.0))
          (rvy (gq:quat 0.0 1.0 0.0 0.0)))

      (setf (uniforms-object-rotation uniforms)
            (gq:q-mat (gv:v4unit (gq:q* (gq:q-rotation rvx (gh:deg-to-rad orx))
                                        (gq:q-rotation rvy (gh:deg-to-rad ory))))))
      (setf (uniforms-camera uniforms)
            (camera-matrix (camera-position-matrix (camera-position camera)
                                                   (camera-direction camera)
                                                   (v4 0.0 1.0 0.0 1.0))
                           100 (/ 1280.0 720.0) :near 0.01 :far 100.0))
      (setf (uniforms-projection uniforms) (gm:fov-m4 100 (/ 1280.0 720.0) 0.01 10.0))
      (setf (uniforms-translation uniforms) (gm:translation-m4 0.0 0.0 -1.3))
      ;;(setf (uniforms-(scaling (gm:scaling-m4 0.5 0.5 0.5))
      (setf (uniforms-light uniforms) (gv:v3unit (camera-position camera))))
    (setenv 'uniforms env uniforms)))

(defun do-render (env)
  (let ((window (getenv 'window env))
        (vao (getenv 'vao env))
        (elements-count 36));;(getenv 'elements-count env)))
    (let ((uniforms (getenv 'uniforms env))
          (program (getenv 'program env)))
      (let ((projection (uniforms-projection uniforms))
            (camera (uniforms-camera uniforms))
            (translation (uniforms-translation uniforms))
            (object-rotation (uniforms-object-rotation uniforms))
            (light (uniforms-light uniforms)))
        (gl:uniform-matrix-4fv (gl:get-uniform-location program "projection")
                               (gm:m4arr projection) nil)
        (gl:uniform-matrix-4fv (gl:get-uniform-location program "translation")
                               (gm:m4arr translation) nil)
        (gl:uniform-matrix-4fv (gl:get-uniform-location program "camera")
                               (gm:m4arr  camera)
                               nil)
        (gl:uniform-matrix-4fv (gl:get-uniform-location program "object_rotation")
                               (gm:m4arr object-rotation) nil)
        (gl:uniformfv (gl:get-uniform-location program "light")
                      light)
        (gl:clear-color 0.3 0.3 0.3 1.0)
        (gl:clear :color-buffer-bit :depth-buffer-bit)
        ;;(break "before draw")
        (display-cube vao elements-count)
        (sdl2:gl-swap-window window)))))


(defun ticks-diff (current next)
  (declare (type (unsigned-byte 32) current next)
           (optimize (speed 3) (safety 0) (debug 0)))
  (let ((current (ldb (byte 32 0) current))
        (next (ldb (byte 32 0) next)))
    (ldb (byte 32 0) (- next current))))

(defun ticks-passed (current next &optional (overlap #xFFFF))
  (declare (type (unsigned-byte 32) current next overlap)
           (optimize (speed 3) (safety 0) (debug 0)))
  (when (>= (ticks-diff current next)  overlap)
    t))

(defun ticks-left (current next &optional (overlap #xFFFF))
  (declare (type (unsigned-byte 32) current next overlap)
           (optimize (speed 3) (safety 0) (debug 0)))
  (let ((tk-left (ticks-diff current next)))
    (when (< tk-left overlap)
      tk-left)))


(defun main-loop (game-env &key event (eps 2000.0) (cps 60.0) (fps 120.0))
  (let ((eticks 0)
        (cticks 0)
        (fticks 0)
        (eint (round (/ 1000.0 eps)))
        (cint (round (/ 1000.0 cps)))
        (fint (round (/ 1000.0 fps))))
    (labels ((act (event)
               (let ((now (sdl2:get-ticks)))
                 (unless (when (ticks-passed now eticks)
                           (setf eticks (+ now eint))
                           (do-events game-env event))
                   (when (ticks-passed now cticks)
                     (setf cticks (+ now cint))
                     (do-things game-env))
                   (when (ticks-passed now fticks)
                     (setf fticks (+ now fint))
                     (do-render game-env))
                   (let ((wait-ticks (ticks-left (min eticks cticks fticks) (sdl2:get-ticks))))
                     (when wait-ticks
                       (sleep (/ wait-ticks 1000))))
                   (act event)))))
      (if event
          (act event)
          (sdl2:with-sdl-event (event)
            (let ((start-ticks (sdl2:get-ticks)))
              (setf eticks start-ticks
                    cticks start-ticks
                    fticks start-ticks)
              (act event)))))))


(defun start-window (game-env)
  (sdl2:with-init (:everything)
    (f-set-gl-version 3 3)
    (sdl2:with-window (win :title "Blocky" :w 1280 :h 720 :flags '(:opengl))
      (sdl2:with-gl-context (gl-context win)
        (sdl2:gl-make-current win gl-context)
        (f-print-gl-version)
        (init-env game-env win)
        (main-loop game-env)))))

(defun game2 ()
  (let ((env (make-game-env)))
    (start-window env)))

