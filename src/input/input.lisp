(in-package #:blocky-game)

(defun key-foo (env down)
  (declare (ignore env down))
  (format t "~&NONE FUNE~%")
  nil)

(defun make-input-map ()
  (make-hash-table))

(defun input-map-set (map key &key (down nil) (up nil))
  (let ((mapped (gethash key map (cons down up))))
    (setf (gethash key map)
          (cons (or down (car mapped) #'key-foo)
                (or up (cdr mapped) #'key-foo)))))

(defun input-map-do (map key &key (down t) env printkey)
  (if printkey (funcall printkey key))
  (let* ((functions (gethash key map (cons #'key-foo #'key-foo)))
         (func (if down
                 (car functions)
                 (cdr functions))))
    (funcall func env down)))

