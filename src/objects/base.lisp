(in-package #:blocky-game)

(defclass entity ()
  ((position  :type 'gv:v3)
   (direction :type 'gv:v3)
   (up        :type 'gv:v3)))

(defclass camera (entity)
  ((far :type 'single-float)
   (near :type 'single-float)
   (horizontal-fov :type 'single-float)
   (vertical-fov :type 'single-float)))

(defclass follow-camera (camera)
  ((followed :type 'entity)
   (follow-distance :type 'single-float)
   (follow-direction :type single-float)))

(defclass physical-object (entity)
  (shape
   mass
   mass-center
   rotational-inertia))

