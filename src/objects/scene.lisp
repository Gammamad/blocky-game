
(defstruct scene-node
  (name)
  (object)
  (children))

(defclass scene ()
  (tree
   meta))
