(in-package #:blocky-game)

(defun prepare-mesh (mesh)
  (let* ((vao (gl:create-vertex-array)))
    (gl:bind-vertex-array vao)
    (let ((buffer (gl:gen-buffer))
	  (triangles (to-gl-array (make-array 6
				 :element-type 'single-float
				 :initial-contents mesh))))
      (gl:bind-buffer :array-buffer buffer)
      (gl:buffer-data :array-buffer :static-draw triangles)
      ;(gl:named-buffer-storage buffer triangles :map-read)
      (gl:vertex-attrib-pointer 0 2 :float :false 0 0)
      (gl:enable-vertex-attrib-array 0)
      vao)))
