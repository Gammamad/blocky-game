#version 440

in Coloring{
  vec4 color;
  vec3 normal;
  vec3 lightv;
};

layout (location = 0) out vec4 fColor;

//uniform vec3 light;

void main ()
{
  
  //float dotv = dot(normal, -lightv);
  //float cosphi = clamp(dotv, 0.0, 1.0);
  //float rel = clamp((1.570795 - acos(cosphi)), 0.0, 0.9);
  //fColor = vec4(normal, 1.0) * rel;
  fColor = color;
}
