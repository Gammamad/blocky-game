#version 440

layout(location = 0)in vec3 vert;
layout(location = 1)in vec3 col;
layout(location = 2)in vec3 norm;

out Coloring{
  vec4 color;
  vec3 normal;
  vec3 lightv;
};


uniform mat4 projection;
uniform mat4 translation;
uniform mat4 object_rotation;
uniform mat4 camera;
uniform vec3 light;

void main()
{
  color = vec4(col, 1.0);
  normal = norm;
  mat4 transform = /*projection */ camera * translation * object_rotation;
  gl_Position = transform * vec4(vert,1.0);
  lightv = light - gl_Position.xyz;
}

