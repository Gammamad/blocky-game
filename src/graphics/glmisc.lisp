(in-package #:blocky-game)

(defun f-set-gl-version (maj min)
    (sdl2:gl-set-attr :context-major-version maj)
    (sdl2:gl-set-attr :context-minor-version min))

(defun f-print-gl-version ()
  (format t "!!!!!!!!!!!!!!!!!OpenGL ~D.~D!!!!!!!!!!!!!!!!!!!!"
	  (gl:get-integer :major-version)
	  (gl:get-integer :minor-version))
  (finish-output))
