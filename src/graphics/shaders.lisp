(in-package #:blocky-game)

(defun load-file (path)
  "Load file as one string(returned)"
  (print path)
  (with-open-file (in path)
    (let ((contents (make-string (file-length in))))
      (read-sequence contents in)
      contents)))

(defun load-shader (shader-type source-path)
  "Load and compile shader of specified type with source in path"
  ;;todo check if tesselation available if tess shadet
  ;;other stuff
  (if source-path
      (let ((shader-str (load-file source-path))
            (shader (gl:create-shader shader-type)))
        (print shader-str)
        (gl:shader-source shader shader-str)
        (gl:compile-shader shader)
        (print (gl:get-shader-info-log shader))
        shader)))
	
(defun make-shader-program (vert frag &key (geom nil) (tess nil))
  "Bind shaders to program and link. Return resulting program"
  (let ((program (gl:create-program)))
    (gl:attach-shader program vert)
    (gl:attach-shader program frag)
    (if geom (gl:attach-shader geom))
    (if tess (gl:attach-shader tess))
    
    (gl:link-program program)

    (print (gl:get-program-info-log program))
    program))

(defun prepare-shader-program ( &key vertex fragment (geometry nil) (tesselation nil) )
  (let* ((vs (load-shader :vertex-shader vertex))
         (gs (load-shader :geometry-shader geometry))
         ;;(ts (load-shader :tesselation-shader tesselation)) ;;TODO: check version and use if possible
	 (fs (load-shader :fragment-shader fragment))
	 (program (make-shader-program vs fs :geom geometry :tess tesselation)))
    (format t "~&vertex shader    : ~D" vs)
    (format t "~&geometry shader  : ~D" gs)
    (format t "~&fragment shader  : ~D" fs)
    (format t "~&program          : ~D~&" program)
    (finish-output)
    ;;(gl:use-program program)
    program))
