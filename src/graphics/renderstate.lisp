
(defgeneric to-gl-array (contents &key (type :int) (default 0.0) (reserve 20)))

(defmethod to-gl-array ((contents list) &key type default reserve)
  (let* ((length (length contents))
         (full-len (coerce (* length (1+ (/ reserve 100))) 'fixnum))
         (gl-array (gl:alloc-gl-array type full-len)))
    (loop for a in contents
       for i from 0 to length
       do(setf (gl:glaref gl-array i) a))
    (loop for i from length to full-len
         do(setf (gl:glaref gl-array i) default)
    gl-array))

(defmethod to-gl-array ((contents array) &key type default reserve)
  (let* ((length (length contents))
         (full-len (coerce (* length (1+ (/ reserve 100))) 'fixnum))
         (gl-array (gl:alloc-gl-array type full-len)))
    (loop for a across contents
       for i from 0 to length
       do(setf (gl:glaref gl-array i) a))
    (loop for i from length to full-len
       do(setf (gl:glaref gl-array i) default)
    gl-array))

(defstruct cube-vbo
  (vbo :type 'fixnum)
  (size :type 'fixnum) ;;in cubes
  (empty-index :type 'fixnum) ;; next free cell
  (empty-list :type 'list)) ;; free cells between

(defstruct cube-vao
  (vao :type 'fixnum)
  (vbo-indeces :type 'fixnum)
  (vbo-attribs :type 'cube-vbo))
  
(defun new-cube-vbo (size)
  (let ((state (new-cube-vbo))
        (vbo (gl:gen-buffer)))
    (setf (cubes-vbo-vbo state) vbo)
    (gl:bind-buffer :array-buffer vbo)
    (gl:buffer-data :array-buffer :static-draw nil :size size)))
    
