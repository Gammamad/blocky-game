(in-package #:blocky-game)

;;(gh:defsequenced-constants blocks
;;  half-pillar)

(defun meta-index-p (thing)
  (and (integerp thing)
       (<= thing 32768)
       (>= thing -32767)))

(deftype meta-index ()
  `(and (integer -32767 32768)
        (satisfies meta-index-p)))

(defun shape-variant-index-p (thing)
  (and (integerp thing)
       (<= thing 16)
       (>= thing 0)))

(deftype shape-variant-index ()
  `(and (integer 0 16)
        (satisfies shape-variant-index-p)))

(defstruct cube-vertex
  (pos (gv:v3) :type (vector single-float)) 
  (norm (gv:v3) :type (vector single-float))
  (uv (gv:v2) :type (vector single-float)))

  
(defstruct cubek-shape
  (variants 0 :type shape-variant-index)
  (verteces nil :type list))

(defvar shapes (make-array 255 :element-type 'cube-shape
                               :initial-element (make-cube-shape)))
(defstruct cubek-material
  (flags 0 :type unsigned-byte);;solid/passable, 


(defstruct cubek
  (material-id 0 :type unsigned-byte)
  (shape-id    0 :type unsigned-byte)
  (variant     0 :type unsigned-byte);;orientation NEWS+UDLR(x16) and variant for multivariant shapes (x16 available)
  (meta-data   0 :type meta-index))

(defconstant +chunk-size+ 16)

(defstruct chunk
  (world-x 0 :type fixnum)
  (world-y 0 :type fixnum)
  (world-z 0 :type fixnum)
  (blocks (make-array (list +chunk-size+ +chunk-size+ +chunk-size+)
                      :element-type 'cubek
                      :initial-element (make-cubek))
   :type (simple-array cubek (* * *)))
  (blocks-meta (make-hash-table :synchronized t) :type hash-table))

(defstruct material
  (texture-index 0 :type fixnum)
  (hardness 0 :type fixnum))

(defun drawn-p (x y z chunk world)
  t)

(gl:define-gl-array-format cubek-format
  (gl:vertex :type :float :components (x y z))
  (gl:tex-coord :type :unsigned-char :components (te-x te-y)))
            

(defun chunk-gl-array (chunk world)
  (let ((chunk-array (gl:alloc-gl-array 'cubek-format (* +chunk-size+
                                                         +chunk-size+
                                                         +chunk-size+
                                                         12 3))))
    (loop for i from 0 below +chunk-size+ do
      (loop for j from 0 below +chunk-size+ do
        (loop for k from 0 below +chunk-size+ do
          (let* ((index (+ i (* j +chunk-size+) (* k +chunk-size+ +chunk-size+))))
            (setf (gl:glaref chunk-array index 'x)
                  (aref (chunk-blocks chunk) i j k)
