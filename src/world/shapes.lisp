(in-package #:blocky-game)

(defun make-shape-vertex (pos norm)
  (make-array 6 :element-type 'single-float
                :initial-contents (append (coerce pos 'list)
                                          (coerce norm 'list))))

(defun gen-pos ()
  ;;(declare (optimize (speed 3) (debug 0) (safety 3)))
  (let* ((axis '(0.0 0.25 0.5 0.75 1.0))
        (places (cons nil nil))
        (last places))
    (dolist (x axis)
      (dolist (y axis)
        (dolist (z axis)
          (setf (cdr last) (cons (gv:v3 x y z)
                                 nil))
          (setf last (cdr last)))))
    (cdr places)))

(defun gen-pn (positions)
  (let* ((e gv:+vector3-x+) ;east
         (n gv:+vector3-z+) ;north
         (w (gv:v3scale e -1)) ;west
         (s (gv:v3scale n -1)) ;south
         (ne (gv:v3unit (gv:v3+ n e)))
         (nw (gv:v3unit (gv:v3+ n w)))
         (sw (gv:v3unit (gv:v3+ s w)))
         (se (gv:v3unit (gv:v3+ s e)))
         (u gv:+vector3-y+) ;up
         (d (gv:v3scale u -1)) ;down
         (ue (gv:v3unit (gv:v3+ u e)))
         (un (gv:v3unit (gv:v3+ u n)))
         (uw (gv:v3unit (gv:v3+ u w)))
         (us (gv:v3unit (gv:v3+ u s)))
         (une (gv:v3unit (gv:v3+ u ne)))
         (unw (gv:v3unit (gv:v3+ u nw)))
         (usw (gv:v3unit (gv:v3+ u sw)))
         (use (gv:v3unit (gv:v3+ u se)))
         (uue (gv:v3unit (gv:v3+ u ue)))
         (uun (gv:v3unit (gv:v3+ u un)))
         (uuw (gv:v3unit (gv:v3+ u uw)))
         (uus (gv:v3unit (gv:v3+ u us)))
         (uune (gv:v3unit (gv:v3+ u une)))
         (uunw (gv:v3unit (gv:v3+ u unw)))
         (uusw (gv:v3unit (gv:v3+ u usw)))
         (uuse (gv:v3unit (gv:v3+ u use)))
         (uee (gv:v3unit (gv:v3+ ue e)))
         (unn (gv:v3unit (gv:v3+ un n)))
         (uww (gv:v3unit (gv:v3+ uw w)))
         (uss (gv:v3unit (gv:v3+ us s)))
         (unene (gv:v3unit (gv:v3+ une ne)))
         (unwnw (gv:v3unit (gv:v3+ unw nw)))
         (uswsw (gv:v3unit (gv:v3+ usw sw)))
         (usese (gv:v3unit (gv:v3+ use se))))
    (let* ((normals (list e n w s ne nw sw se u d
                        ue un uw us une unw usw use
                        uue uun uuw uus uune uunw uusw uuse
                        uee unn uww uss unene unwnw uswsw usese))
           (places (cons nil nil))
           (last places))
      (dolist (n normals)
        (dolist (p positions)
          (setf (cdr last) (cons (make-shape-vertex p n) nil))
          (setf last (cdr last))))
      (cdr places))))


(defvar *positions* (gen-pn (gen-pos)))


(defun get-shape-point (points p n)
  (aref points (+ p (* n 125))))
