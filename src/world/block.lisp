(in-package #:blocky-game)

(deftype block ()
  `unsigned-int)

(defun block-shape (blk)
  ;;(declare (tpye blk unsigned-int))
  (ldb (byte 8 24) blk)) ;; 8 bits shape

(defun block-orientation (blk)
  ;;(declare (tpye blk unsigned-int))
  (ldb (byte 4 20) blk)) ;; 4 bits orientation nwsr+udlr

(defun block-light (blk)
  ;;(declare (tpye blk unsigned-int))
  (ldb (byte 4 16) blk)) ;; 4 bits mc like lighting

(defun block-material (blk)
  ;;(declare (tpye blk unsigned-int))
  (ldb (byte 8 8) blk)) ;; 8 bits material

(defun block-meta (blk)
  ;;(declare (tpye blk unsigned-int))
  (ldb (byte 8 0) blk)) ;; 8 bit metadata

(defun block-meta-extendedp (meta)
  (= meta 255))



(defconstant +chunk-dimmension+ 16)
(defconstant +chunk-block-count+ (* +chunk-dimmension+
                                    +chunk-dimmension+
                                    +chunk-dimmension+))

(deftype chunk ()
  `(simple-array 'unsigned-int
                 (* +chunk-dimmension+
                    +chunk-dimmension+
                    +chunk-dimmension+)))

(defun make-chunk ()
  (make-array +chunk-block-count+
              :element-type 'unsigned-int
              :initial-value 0))


(defun shape-vertices (shape side)
  nil)

(defun shape-bb (shape)
  nil)

(defun shape-collider (shape)
  nil)
